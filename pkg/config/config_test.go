// config holds configuration for all the jnk application components
package config

import "testing"

func Test_extractIndex(t *testing.T) {
	type args struct {
		name   string
		prefix string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			name:    "same",
			args:    args{"bridge", "bridge"},
			want:    0,
			wantErr: false,
		},
		{
			name:    "openvpn-1",
			args:    args{"openvpn-1", "openvpn"},
			want:    1,
			wantErr: false,
		},
		{
			name:    "openvpn-200",
			args:    args{"openvpn-200", "openvpn"},
			want:    200,
			wantErr: false,
		},
		{
			name:    "openvpn-",
			args:    args{"openvpn-", "openvpn"},
			want:    0,
			wantErr: true,
		},
		{
			name:    "openvpn-aa",
			args:    args{"openvpn-aa", "openvpn"},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := extractIndex(tt.args.name, tt.args.prefix)
			if (err != nil) != tt.wantErr {
				t.Errorf("extractIndex() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("extractIndex() = %v, want %v", got, tt.want)
			}
		})
	}
}
