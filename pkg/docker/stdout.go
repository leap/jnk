package docker

import (
	"io"
	"os"

	"github.com/docker/docker/pkg/jsonmessage"
	"github.com/moby/term"
)

func wrapStdout(out io.ReadCloser) {
	termFd, isTerm := term.GetFdInfo(os.Stderr)
	jsonmessage.DisplayJSONMessagesStream(out, os.Stderr, termFd, isTerm, nil)
}
