package docker

import (
	"bytes"
	"fmt"
	"io"
	"math/rand"
	"os"
	"unicode"

	"0xacab.org/atanarjuat/jnk/pkg/config"
)

var (
	HostName = "localhost"
)

func InitPKI() {
	cfg := config.NewConfigFromViper()
	RunCmdInContainer(cfg.ImageOpenVPN, nil, []string{"ovpn_genconfig", "-u", fmt.Sprintf("udp://%s", HostName)}, &CmdConfig{}, nil)
	RunCmdInContainer(cfg.ImageOpenVPN, nil, []string{"ovpn_initpki", "nopass"}, &CmdConfig{Interactive: true}, nil)
}

func GenerateClientCert() []byte {
	cfg := config.NewConfigFromViper()
	var buf bytes.Buffer
	clientName := randomString(12)
	RunCmdInContainer(
		cfg.ImageOpenVPN,
		nil,
		[]string{
			"easyrsa", "--batch", "build-client-full", clientName, "nopass",
		},
		&CmdConfig{
			Silent: true,
		},
		nil,
	)
	RunCmdInContainer(
		cfg.ImageOpenVPN,
		nil,
		[]string{
			"ovpn_getclient", clientName,
		},
		&CmdConfig{
			Silent: true,
		},
		&buf,
	)
	// FIXME why am I getting control characters here in the first place?
	var cleanedBuffer bytes.Buffer
	for _, r := range buf.String() {
		if unicode.IsPrint(r) || r == '\n' {
			cleanedBuffer.WriteRune(r)
		}
	}
	io.Copy(os.Stdout, &cleanedBuffer)
	return buf.Bytes()
}

func randomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyz"

	result := make([]byte, length)
	for i := range result {
		result[i] = charset[rand.Intn(len(charset))]
	}
	return string(result)
}
