package docker

const (
	openVPNImage = "registry.0xacab.org/leap/openvpn-docker-standalone/openvpn-docker-standalone"
	menshenImage = "registry.0xacab.org/leap/menshen/menshen"
	datavolume   = "leap-vpn-data"
)
